<?php

namespace App\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class TltHomeController
 */
class HomeController extends Controller
{
    /**
     * Afficher la page accueil
     * @return string
     */
    public function indexAction()
    {
        $_response_manager = $this->get('tvk.manager.response');
        $message           = 'No data was Found';
        $_response         = $_response_manager->getWsResponse(false, $message);

        return $_response;
    }
}
