<?php

namespace App\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TltHomeController
 */
class LoginController extends Controller
{
    /**
     * Afficher la page accueil
     * @return string
     */
    public function loginAction(Request $request)
    {
        $_response_manager = $this->get('tvk.manager.response');
        $message           = 'Post Work :)';
        $_response         = $_response_manager->response('donnees_api', ['name' => 'Rakoto Test', 'message' => $message]);

        return $_response;
    }

    /**
     * Afficher la page accueil
     * @return string
     */
    public function resettingPasswordAction()
    {
        die('To do list');
    }

}
