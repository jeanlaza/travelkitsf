<?php

namespace App\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class TltHomeController
 */
class RegisterController extends Controller
{
    /**
     * Afficher la page accueil
     * @return string
     */
    public function indexAction()
    {
        die('register action');
        // Recuperer manager
        $_common_manager = $this->get(ServiceName::SRV_METIER_COMMON);

        // Recuperer tout les matieres
        $_matieres = $_common_manager->getAllEntities(EntityName::TLT_MATIERE);
        $_niveaux = $_common_manager->getAllEntities(EntityName::TLT_NIVEAU);

        return $this->render('FrontBundle:TltHome:index.html.twig', array(
            'matieres' => $_matieres,
            'niveaux' => $_niveaux,
        ));
    }
}
