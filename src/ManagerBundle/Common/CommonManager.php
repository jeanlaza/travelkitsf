<?php

namespace App\ManagerBundle\Common;

use App\Lle\Service\MetierManagerBundle\Entity\LleInfoChauffeur;
use App\Lle\Service\MetierManagerBundle\Utils\EntityName;
use App\Lle\Service\MetierManagerBundle\Utils\EntrepriseStatutName;
use App\Lle\Service\MetierManagerBundle\Utils\RoleName;
use App\Lle\Service\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CommonManager
{
    private $_entity_manager;
    private $_container;
    private $_authorization_checker;
    private $_token_storage;

    public function __construct(EntityManager $_entity_manager, Container $_container, AuthorizationCheckerInterface $_authorization_checker, TokenStorageInterface $_token_storage)
    {
        $this->_entity_manager        = $_entity_manager;
        $this->_container             = $_container;
        $this->_authorization_checker = $_authorization_checker;
        $this->_token_storage         = $_token_storage;
    }

    /**
     * Recupere l utilisateur connecte
     * @return null|object|string
     */
    public function getUserConnected()
    {
        $_user_connected = null;
        $_user           = $this->_token_storage->getToken()->getUser();
        if ($_user != 'anon.') {
            $_user_connected = $this->_token_storage->getToken()->getUser();
        }

        return $_user_connected;
    }

    /**
     * Recuperer l entrepise id connecte
     * @return integer
     */
    public function getCurrentEntrepriseId()
    {
        $_entreprise_id = 0;

        if ($this->_authorization_checker->isGranted('IS_AUTHENTICATED_FULLY') && $this->getUserConnected()) {
            if ($this->getUserConnected()->getLleEntreprise()) {
                $_entreprise_id = $this->getUserConnected()->getLleEntreprise()->getId();
            }
            $_session = new Session();

            if ($_session->get('id_entreprise')) {
                $_entreprise_id = $_session->get('id_entreprise');
            }
        }

        return $_entreprise_id;
    }

    /**
     * Recuperer l entrepise  connecte
     * @return integer
     */
    public function getCurrentEntreprise()
    {
        $_entreprise = null;

        if ($this->_authorization_checker->isGranted('IS_AUTHENTICATED_FULLY')) {
            if ($this->getUserConnected()->getLleEntreprise()) {

                $_entreprise = $this->getUserConnected()->getLleEntreprise();
            }
        }

        return $_entreprise;
    }

    /**
     * Recuperer le role
     * @return int
     */
    public function getRoleId()
    {
        $_role_id = 0;
        if ($this->getUserConnected()) {
            $_user_role = $this->getUserConnected()->getLleRole();

            if ($_user_role)
                $_role_id = $_user_role->getId();
        }

        return $_role_id;
    }

    /**
     * Envoyer un email
     * @param $_recipient
     * @param $_subjet
     * @param $_template
     * @param $_data_param
     * @param null $_cc
     * @return bool
     * @throws \Twig\Error\Error
     */
    public function sendMail($_recipient, $_subjet, $_template, $_data_param, $_cc = null)
    {
        $_email_body         = $this->_container->get('templating')->renderResponse($_template, $_data_param);
        $_from_firstname     = $this->_container->getParameter('from_firstname');
        $_from_email_address = $this->_container->getParameter('from_email_address');

        $_email_body = implode("\n", array_slice(explode("\n", $_email_body), 4));
        $_message    = (new \Swift_Message($_subjet))
            ->setFrom(array($_from_email_address => $_from_firstname))
            ->setTo($_recipient)
            ->setBody($_email_body);

        if ($_cc != null) {
            $_message = (new \Swift_Message($_subjet))
                ->setFrom(array($_from_email_address => $_from_firstname))
                ->setTo($_recipient)
                ->setCc($_cc)
                ->setBody($_email_body);
        }

        $_message->setContentType("text/html");
        $_result = $this->_container->get('mailer')->send($_message);

        $_headers = $_message->getHeaders();
        $_headers->addIdHeader('Message-ID', uniqid() . "@domain.com");
        $_headers->addTextHeader('MIME-Version', '1.0');
        $_headers->addTextHeader('X-Mailer', 'PHP v' . phpversion());
        $_headers->addParameterizedHeader('Content-type', 'text/html', ['charset' => 'utf-8']);

        if ($_result) {
            return true;
        }

        return false;
    }

    /** Enregistrement entreprise id dans la session
     * @param $_entreprise
     * @return bool
     */
    public function setSessionEntrepriseId($_entreprise)
    {
        $_session = new Session();

        // set and get session attributes
        $_session->set('id_entreprise', $_entreprise->getId());
        $_session->set('nom_entreprise', $_entreprise->getEntrNom());

        return true;
    }

    /**
     * Suppresion session entreprise id
     * @return bool
     */
    public function removeSessionEntrepriseId()
    {
        $_session = new Session();
        // set and get session attributes
        $_session->remove('id_entreprise');
        $_session->remove('nom_entreprise');
        return $_session;
    }

    /**
     * Restaurer une entreprise ou un utilisateur
     * @param $_object_to_restore
     * @param $_type
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function restoreData($_object_to_restore, $_type)
    {
        $_user = EntityName::USER;
        if ($_object_to_restore && $_type == 'user') {
            $_user_role = $_object_to_restore->getLleRole();
            if ($_user_role->getId() == RoleName::ID_ROLE_SUPERADMIN_ENTREPRISE) {
                $_dql   = "UPDATE $_user usr
                          SET usr.deletedAt = NULL
                          WHERE usr.lleEntreprise = :entreprise";
                $_query = $this->_entity_manager->createQuery($_dql);
                $_query->setParameter('entreprise', $_object_to_restore->getLleEntreprise());
                $_query->execute();
            }
        } else {
            $_dql   = "UPDATE $_user usr
                      SET usr.deletedAt = NULL
                      WHERE usr.lleEntreprise = :entreprise";
            $_query = $this->_entity_manager->createQuery($_dql);
            $_query->setParameter('entreprise', $_object_to_restore);
            $_query->execute();
        }

        $_object_to_restore->setDeletedAt(null);
        $this->_entity_manager->flush();

        return true;
    }

    /**
     * Verifier si l abonnement de l entreprise n est pas bloque
     * @return bool
     */
    public function checkSubscription()
    {
        $_is_valid           = true;
        $_current_entreprise = $this->getCurrentEntreprise();

        if ($_current_entreprise) {
            $_statut_entreprise = $_current_entreprise->getLleEntrepriseStatut();
            if ($_statut_entreprise) {
                $_statut_bloque = EntrepriseStatutName::ID_ENTR_BLOQUE;
                if ($_statut_entreprise->getId() == $_statut_bloque)
                    $_is_valid = false;
            }
        }

        return $_is_valid;
    }

    /**
     * Ajouter un message flash
     * @param string $_type
     * @param string $_message
     * @return mixed
     */
    public function setFlash($_type, $_message)
    {
        return $this->_container->get('session')->getFlashBag()->add($_type, $_message);
    }

    /**
     * Recuperer le repository specifique
     * @param string $_entity_name
     * @return array
     */
    public function getRepository($_entity_name)
    {
        return $this->_entity_manager->getRepository($_entity_name);
    }

    /**
     * Recuperer tout les entites specifiques
     * @param string $_entity_name
     * @return array
     */
    public function getAllEntities($_entity_name)
    {
        return $this->getRepository($_entity_name)->findBy(array(), array('id' => 'DESC'));
    }

    /**
     * Recuperer tout les entites specifiques par filtre et ordre
     * @param string $_entity_name
     * @param array $_filter
     * @param array $_order
     * @return array
     */
    public function getAllEntitiesByOrder($_entity_name, $_filter, $_order)
    {
        return $this->getRepository($_entity_name)->findBy($_filter, $_order);
    }

    /**
     * Recuperer tout les entites specifiques par filtre
     * @param string $_entity_name
     * @param array $_filter
     * @return array
     */
    public function getEntityByFilter($_entity_name, $_filter)
    {
        return $this->getRepository($_entity_name)->findBy($_filter);
    }

    /**
     * Recuperer tout un entite specifique
     * @param string $_entity_name
     * @param array $_filter
     * @return array
     */
    public function findOneEntityByFilter($_entity_name, $_filter)
    {
        return $this->getRepository($_entity_name)->findOneBy($_filter);
    }

    /**
     * Recuperer un entite specifique par identifiant
     * @param string $_entity_name
     * @param integer $_id
     * @return array
     */
    public function getEntityById($_entity_name, $_id)
    {
        return $this->getRepository($_entity_name)->find($_id);
    }

    /**
     * Enregistrer un entite specifique
     * @param Object $_entity
     * @param string $_action
     * @return object
     */
    public function saveEntity($_entity, $_action)
    {
        if ($_action == 'new') {
            $this->_entity_manager->persist($_entity);
        }
        $this->_entity_manager->flush();
        return $_entity;
    }

    /**
     * Supprimer un entite specifique
     * @param Object $_entity
     * @return boolean
     */
    public function deleteEntity($_entity)
    {
        $this->_entity_manager->remove($_entity);
        $this->_entity_manager->flush();

        return true;
    }

    /**
     * Suppression multiple des entites specifiques
     * @param string $_entity_name
     * @param array $_ids
     * @return boolean
     */
    public function deleteGroupEntity($_entity_name, $_ids)
    {
        if (count($_ids)) {
            foreach ($_ids as $_id) {
                $_entity = $this->getEntityById($_entity_name, $_id);
                $this->deleteEntity($_entity);
            }
        }

        return true;
    }

    /**
     * Vérifier si l'utilisateur connecté appartient bien à l'entreprise (ou surperadmin globale)
     * @param $_entity_name
     * @param $_entity
     * @return bool
     */
    public function checkEntrepriseIdConnectedByEntity($_entity_name, $_entity)
    {
        $_user_connected      = $this->_container->get('security.token_storage')->getToken()->getUser();
        $_user_role_connected = $_user_connected->getLleRole()->getId();

        $_is_valid = false;

        $_dql = "SELECT entity 
                 FROM $_entity_name  entity  
                 WHERE entity.lleEntreprise = :entreprise_id
                 AND entity.id = $_entity";

        $_query = $this->_entity_manager->createQuery($_dql);
        $_query->setParameter('entreprise_id', $this->getCurrentEntreprise());

        $_result = $_query->getOneOrNullResult();
        if ($_result || in_array($_user_role_connected, [ RoleName::ID_ROLE_SUPERADMIN, RoleName::ID_ROLE_ADMIN ]))
            $_is_valid = true;

        return $_is_valid;
    }

	/**
	 * Recuperation entity selon entreprise connected
	 * @param $_entity_name
	 * @param $_entity
	 * @return mixed
	 */
    public function getAllEntityByEntrepriseConnected($_entity_name){

		$_dql = "SELECT entity FROM $_entity_name  entity  WHERE entity.lleEntreprise = :entreprise_id";

		$_query = $this->_entity_manager->createQuery($_dql);
		$_query->setParameter('entreprise_id', $this->getCurrentEntrepriseId());

		$_result =  $_query->getResult();

		return $_result;
	}

	public function tryImport()
	{
		$_chauffeur = new LleInfoChauffeur();
		$_chauffeur->setChfCin('123456781');
		$_chauffeur->setChfCatPermis('ABCD');
		$_user = new User();
		$_user->setEmail('drvaly@yahoo.fr');
		$_user->setPassword('drvaly@yahoo.fr');
		$_user->setUsername('drvaly@yahoo.fr');
		$_user->setUsrLastname('drvaly@yahoo.fr');
		$_user->setLleInfoChauffeur($_chauffeur);
		$this->_entity_manager->persist($_chauffeur);
		$this->_entity_manager->persist($_user);
		$this->_entity_manager->flush();
		die('success');
	}
}