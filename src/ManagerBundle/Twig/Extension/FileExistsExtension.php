<?php

namespace App\ManagerBundle\Twig\Extension;

class FileExistsExtension extends \Twig_Extension
{
    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('slugify', array($this, 'slugFilter')),
        );
    }

    public function slugFilter($textToSlug) {
        $slugger = new Util();
        return $slugger->slugify($textToSlug);
    }

    /**
     * Return the functions registered as twig extensions
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('file_exists', 'file_exists'),
        );
    }

    public function getName()
    {
        return 'tvk_twig';
    }
}
