<?php

namespace App\ManagerBundle\Utils;

/**
 * Class EntityName
 * Classe qui contient les noms constante de tout les entités
 */
class EntityName
{
    const USER = 'UserBundle:User';
    const LLE_PIECE = 'MetierManagerBundle:LlePiece';
    const LLE_PIECE_TYPE = 'MetierManagerBundle:LlePieceType';
    const LLE_ENTR_RAISON_SOCIAL = 'MetierManagerBundle:LleEntrepriseRaisonSocial';
    const LLE_USER_ROLE = 'MetierManagerBundle:LleRole';
    const LLE_CENTRE = 'MetierManagerBundle:LleCentre';
    const LLE_INFO_CHAUFFEUR = 'MetierManagerBundle:LleInfoChauffeur';
    const LLE_FORMATION = 'MetierManagerBundle:LleFormation';
    const LLE_CHAUFFEUR_FORMATION = 'MetierManagerBundle:LleChauffeurFormation';
    const LLE_VEHICULE_MARQUE = 'MetierManagerBundle:LleVehiculeMarque';
    const LLE_VEHICULE = 'MetierManagerBundle:LleVehicule';
    const LLE_VEHICULE_MODELE = 'MetierManagerBundle:LleVehiculeModele';
    const LLE_PLACE = 'MetierManagerBundle:LlePlace';
    const LLE_TRAJET = 'MetierManagerBundle:LleTrajet';
    const LLE_VEHICULE_TRAJET = 'MetierManagerBundle:LleVehiculeTrajet';
    const LLE_VISITE_MEDICALE       = 'MetierManagerBundle:LleVisiteMedicale';
    const LLE_PIECE_VEHICULE_MODELE = 'MetierManagerBundle:LlePieceVehiculeModele';
    const LLE_PIECE_VEHICULE = 'MetierManagerBundle:LlePieceVehicule';
    const LLE_VISITE = 'MetierManagerBundle:LleVisite';
    const LLE_VEHICULE_VISITE = 'MetierManagerBundle:LleVehiculeVisite';
    const LLE_CONSOMMABLE = 'MetierManagerBundle:LleConsommable';
    const LLE_CONSOMMABLE_TYPE = 'MetierManagerBundle:LleConsommableType';
    const LLE_CONGE = 'MetierManagerBundle:LleConge';
    const LLE_CHAUFFEUR_NOTIFICATION = 'MetierManagerBundle:LleChauffeurNotification';
    const LLE_CHAUFFEUR_INCIDENT = 'MetierManagerBundle:LleChauffeurIncident';
    const LLE_MAINTENANCE = 'MetierManagerBundle:LleMaintenance';
    const LLE_IMAGE_CHEUFFEUR_INCIDENT = 'MetierManagerBundle:LleImageChauffeurIncident';
    const LLE_URGENCE_TYPE = 'MetierManagerBundle:LleUrgenceType';
    const LLE_URGENCE = 'MetierManagerBundle:LleUrgence';
    const LLE_ASSURANCE = 'MetierManagerBundle:LleAssurance';
    const LLE_VEHICULE_ASSURANCE = 'MetierManagerBundle:LleVehiculeAssurance';
    const LLE_ABSENCE = 'MetierManagerBundle:LleAbsence';
    const LLE_REMORQUE = 'MetierManagerBundle:LleRemorque';
    const LLE_CHAUFFEUR_POSITION = 'MetierManagerBundle:LleChauffeurPosition';
    const LLE_ENTREPRISE = 'MetierManagerBundle:LleEntreprise';
    const LLE_MESSAGE_NEWSLETTER = 'MetierManagerBundle:LleMessageNewsletter';
    const LLE_ENTREPRISE_STATUT = 'MetierManagerBundle:LleEntrepriseStatut';
    const LLE_CHAUFFEUR_APP_HISTORIQUE = 'MetierManagerBundle:LleChauffeurAppHistorique';
    const LLE_VEHICULE_HISTORIQUE = 'MetierManagerBundle:LleVehiculeHistorique';
    const LLE_FACTURE = 'MetierManagerBundle:LleFacture';
    const LLE_TARIF = 'MetierManagerBundle:LleTarif';
    const LLE_GENRE = 'MetierManagerBundle:LleGenre';
    const LLE_ESSIEUX = 'MetierManagerBundle:LleEssieux';
    const LLE_ORGANISME_SANITAIRE = 'MetierManagerBundle:LleOrganismeSanitaire';
    const LLE_BAREMAGE = 'MetierManagerBundle:LleBaremage';
    const LLE_COMPAGNIE = 'MetierManagerBundle:LleCompagnie';
    const LLE_CLIENT= 'MetierManagerBundle:LleClient';
    const LLE_AGREMENTATION = 'MetierManagerBundle:LleAgrementation';
    const LLE_CMS = 'MetierManagerBundle:LleCms';
    const LLE_FORMULE = 'MetierManagerBundle:LleFormule';
	const VM_ACCESS_TOKEN = 'MetierManagerBundle:AccessToken';
}
