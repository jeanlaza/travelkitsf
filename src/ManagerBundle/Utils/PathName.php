<?php

namespace App\ManagerBundle\Utils;

/**
 * Class PathReportingName
 * Classe qui contient les noms constante des répertoires reporting
 */
class PathName
{
    const UPLOAD_IMAGE_USER = '/upload/user/';
    const UPLOAD_JOINTE = '/upload/jointe/';
    const UPLOAD_IMAGE_INCIDENT = '/upload/incident/';
    const UPLOAD_CSV_FILE = '/upload/import/';
    const UPLOAD_IMAGE_SEO = '/upload/seo/';
}
