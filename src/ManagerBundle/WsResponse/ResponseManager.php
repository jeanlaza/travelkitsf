<?php

namespace App\ManagerBundle\WsResponse;

use Symfony\Component\HttpFoundation\JsonResponse;

class ResponseManager
{
    /**
     * Retourner le reponse pour chaque ws
     * @param $_status
     * @param $_message
     * @param $_data
     * @return array
     */
    public function getWsResponse($_status, $_message, $_data = null){
        $_response = [
            'status'  => $_status,
            'message' => $_message
        ];

        if($_data !== null)
            $_response['data'] = $_data;

        return new JsonResponse($_response, 200, [
            "Content-Type" => 'json',
            "Access-Control-Allow-Origin" => "*",
            "Access-Control-Allow-Headers" => "Origin, X-Requested-With, Content-Type, Accept"
        ]);
    }

    /**
     * @param $_name
     * @param $_data
     *
     * @return JsonResponse
     */
    public function response($_name, $_data)
    {
        $_list = new JsonResponse();
        $_list->setData(array($_name => $_data));
        $_list->setStatusCode(200);
        $_list->headers->set('Content-Type', 'application/json');
        $_list->headers->set('Access-Control-Allow-Origin', '*');
        return $_list;
    }
}